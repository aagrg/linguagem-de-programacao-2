# Exemplo de Aula de Linguagem de programação   II

# Nota de aula

Nome: Alexandre
Ra: 1918474

Linux e GitLab

1. 15/08/2022

# Linguagem de programação II
# php

```php
<?php 
echo "hello world";

```

arquivo criado php criado na pasta PHP AULA

# git 
## aprendendo commit

obs: comando na área que o arquivo foi salvo.

```sh
git init
git add . --> adicionar o arquivo
git commit -m "hello world"  --> documentar mudanças 
git log --> listar todos os commits

git commit --amend --> alterar o log 

php -S 0.0.0.0:8000
```

No browser --> localhost:8000
requisição de index.php --> Hello Word

obs: terminal código 404 (erro)
     terminar código 200 (deu certo)



1. remotos

Para fazer a interação dos repositórios (máquina/local com o do gitlab) --> sincronizar os repositórios

git remote add nome url
git remote  remove nome      --> caso de erro na url
gitremote set-url nome nova-url

Uso do repositório de notas de aula:

git pull nome --rebase              
git pull origin main --rebase             --> antes do push para mesclagem dos códigos (rebase)

git push origin main --> no caso, trocar main por master   (ainda não entendi o por quê?)   

2. SSH x HTTPS

SSH protocolo de rede cripitográfico, criação de chave para identificação do usuário; --> chave (cliente)
HTTPS protocolo padrão de criptografia da web, porém com a identificação do lado da rede; --> senha



3. Exercícios - Variáveis
```
<?php
		
          //entregador
		$nome = "Alexandre";
		//int
		$idade = 30 ;
		//float
		$cel = 99996666;
	?>
```
4. Exercícios - Funções


```
<?php foreach($chamados as $chamado) { ?>

<?php
     $chamado_dados = explode('#', $chamado);
     if (count($chamado_dados) < 3) {
          continue;
     }?>
     <div class="card mb-3 bg-light">
          <div class="card-body">
               <h5 class="card-title"><?=$chamado_dados[0]?></h5>
               <h6 class="card-subtitle mb-2 text-muted"><?=$chamado_dados[1]?></h6>
               <p class="card-text"><?=$chamado_dados[2]?></p>

          </div>
     </div>

     <?php } ?>

```
5. Testes


```
<?php

use PHPUnit\Framework\TestCase;
use Alexandre\EntregaUnimar\User;

class UserTest extends TestCase {

    public function testBelongToAdmin() {
        $user = new User(0, null, ['admin']);
        $this->assertTrue($user->belongsTo('admin'));
        $this->assertFalse($user->belongsTo('user'));
    }

    public function testBelongToMany() {
        $user = new User(0, null, ['admin','user']);
        $this->assertTrue($user->belongsTo('admin'));
        $this->assertTrue($user->belongsTo('user'));
    }
}
```

6. Teste rota/frete

```
<?php

class Frete {
    function __construct(private array $pontos) {

    }

    function totalDistance() {
        $distance = 0;
        $pontoAnterior = $this->pontos[0];
        foreach($this->pontos as $ponto) {
            $distance += $ponto->distanceTo($pontoAnterior);
            $pontoAnterior = $ponto;
        }
        return $distance;
    }
}


    class Ponto {
        
        function __construct(
            private $longitude = null,
            private $latitude = null,
        ){
            
        }

        function __get($atributo) {
            return $this->$atributo;
        }

        function __set($atributo, $valor) {
            $this->$atributo = $valor;
        }

        function distanceTo(Ponto $b) {
            //R * arccos (sin (lata) * sin (latB) + cos (lata) * cos (latB) * cos (Lona-lonB)) 

            $dist =  (6372.795477598 * acos((sin($this->latitude) * sin($b->latitude)) + (cos($this->latitude) * cos($b->latitude) * cos($this->longitude - $b->longitude))));
            $dist = number_format(deg2rad($dist), 2, '.', '');
            return $dist;

        }
    

    }

    
    $f = new Frete([
        new Ponto(-22.226966718332033,-49.955911723414076),
        new Ponto(-22.224941092188033,-49.9564197842759),
        
    ]);
    echo $f->totalDistance();   //erro de ~100 m
    echo '<br>';
    $f1 = new Frete([
        new Ponto(-22.226966718332033,-49.955911723414076),
        new Ponto(-22.226966718332033,-49.955911723414076),
        
    ]);
    echo $f1->totalDistance();  // 0

``` 
